# firststep_opensource_document_manager

## Getting started

1. Install WAMP (windows) or LAMP (Linux).

2. Navigate to index.php in a browser.

## Content

To edit the raw contents, edit `array_data.json`.

## User (Read-only)

Navigate to: index.php

Navigate through the navigation tree, and select a page to view the contents.

## User (Admin)

Navigate to edit.php

Enter username / password: admin / admin

Navigate through the navigation tree, and select a page to view the contents.

Edit the page in the main window, and click `Save` to save changes.

Click `Add` button to add a page.

Click `Edit Menu` button to edit a menu.

## TODO

1. Drag & Drop image uploader
2. Bullet points to MCE
3. Audit trail / page history.
