<?php
session_start();

include_once("vars.php");

if (isset($_GET['logout'])) {
    session_destroy();
    header('Location: index.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    # print_r($APP_USERS); print("<br>");
    # print($APP_USERS[$username]); print("<br>");
    # print($password); print("<br>");

    // Lookup table for authentication
    if (isset($APP_USERS[$username]) && $APP_USERS[$username] === $password) {
        if ($username === 'editor') {
            $_SESSION['user_role'] = 'editor';
        } elseif ($username === 'developer') {
            $_SESSION['user_role'] = 'developer';
        } elseif ($username === 'integrator') {
            $_SESSION['user_role'] = 'integrator';
        } else {
            $_SESSION['user_role'] = 'open';
        }
    } else {
        $_SESSION['user_role'] = 'open';
    }

    if ($_SESSION['user_role'] == 'editor'){
      # print_r($_SESSION['user_role']);
      # exit();
      header('Location: edit.php');
    }
    else {
      # print_r($_SESSION['user_role']);
      # exit();
      header('Location: index.php');
    }

    exit();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>FirstStep.ai Documentation Hub</title>
    <link rel="stylesheet" href="static/css/style.css" referrerpolicy="origin" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="static/img/favicon.png">
</head>
<body>

  <div id="loginModal">
      <div class="modal-container">
        <form method="POST">
          <div class="modal-header">
              <h3>Login</h3>
          </div>
          <div class="modal-body">
                <label for="username">Username:</label>
                <input type="text" id="username" name="username"><br><br>
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" required><br><br>
          </div>
          <div class="modal-footer">
              <button type="submit">Login</button>
          </div>
        </form>
      </div>
  </div>

</body>
</html>
