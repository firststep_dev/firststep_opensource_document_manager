<?php
session_start();

if ($_SESSION['user_role'] !== 'editor') {
    header('Location: index.php');
    exit();
}

if (in_array($_SESSION['user_role'], ["editor", "developer", "integrator"])) {
    $loginLogoutButton = '<a href="auth.php?logout=true">Logout</a>';
} else {
    $loginLogoutButton = '<a href="auth.php">Login</a>';
}
?>

<?php

include_once("vars.php");
ini_set('display_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>

<?php

function log_message($message) {
    $log_file = 'logs/index.log';
    $current_time = date('Y-m-d H:i:s');

    // Get the full URL
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
    $url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $log_entry = $current_time . ' - ' . $message . ' - URL: ' . $url . "\n";
    file_put_contents($log_file, $log_entry, FILE_APPEND);
}

log_message("Page call (edit.php)");

?>

<?php include_once("header.php");?>

  <div class="subheader">
    Product:
    <select id="product-select" disabled>
        <option value="" selected>CRATE TRACKER</option>
    </select>
    Version:
    <select id="version-select" disabled>
        <option value="" selected>2024</option>
    </select>
    <div style="width:100%; display: flex; justify-content: right;">
      <span class="timestamp" id="add_page">
        <span><button id='addButton' style='margin-right:10px;'>Add Page</button>
      </span>
      <span class="timestamp" id="edit_page">
        <span><button id='editButton' style='margin-right:10px;'>Edit Menu</button>
      </span>
      <span class="timestamp" id="save_timestamp" style="<?= isset($_GET['saved']) && $_GET['saved'] == 'true' ? '' : 'display: none;'; ?>">
        Successfully saved: <?= date('Y-m-d H:i:s'); ?>
      </span>
      <?php if (isset($_GET['p'])) { ?>
        <span class="timestamp" id="save_button" style="<?= isset($_GET['saved']) && $_GET['saved'] == 'true' ? 'display: none;' : ''; ?>">
          <button type="button" onclick="saveContent()">Save</button>
        </span>
      <?php } ?>
    </div>
  </div>


<?php

function read_json($filename) {
    if (!file_exists($filename)) {
        // Create a blank template and save it if the file doesn't exist
        $data = ['rows' => []];
        file_put_contents($filename, json_encode($data, JSON_PRETTY_PRINT));
        return $data;
    }
    $json = file_get_contents($filename);
    return json_decode($json, true);
}

// Function to reload the page with a specific 'p' value
function reloadPageWithParam($pValue) {
    $baseUrl = $_SERVER['PHP_SELF'];
    $queryParams = $_GET;
    $queryParams['p'] = $pValue;
    $queryString = http_build_query($queryParams);
    $fullUrl = $baseUrl . '?' . $queryString;
    header("Location: $fullUrl");
    exit; // Make sure to call exit after header redirection
}

$menu_json = read_json('json/menu.json');

// Check if items need to be edited
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edittargetID'], $_POST['editparentId'], $_POST['editName'], $_POST['editSave'], $_POST['editLevel'])) {
    $pageId = $_POST['edittargetID'];
    $parentId = $_POST['editparentId'];
    $newName = $_POST['editName'];
    $newLevel = $_POST['editLevel'];
    foreach ($menu_json as &$item) {
        if ($item['id'] == $pageId) {
            $item['parent_id'] = $parentId;
            if (strlen($newName) > 0)
            {
              $item['name'] = $newName;
            }
            $item['level'] = $newLevel;
            break;
        }
    }
    file_put_contents('json/menu.json', json_encode($menu_json, JSON_PRETTY_PRINT));
}

// Check if new items need to be added
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['addparentId'], $_POST['addnewName'], $_POST['addLevel'])) {
    $parentId = $_POST['addparentId'];
    $newName = $_POST['addnewName'];
    $newLevel = $_POST['addLevel'];
    $highestId = 0;
    foreach ($menu_json as $item) {
        if ($item['id'] > $highestId) {
            $highestId = $item['id'];
        }
    }
    $newId = $highestId + 1;
    $menu_json[] = array(
        'id' => $newId,
        'parent_id' => $parentId,
        'name' => $newName,
        'level' => $newLevel // Save the new level to JSON
    );
    file_put_contents('json/menu.json', json_encode($menu_json, JSON_PRETTY_PRINT));
    reloadPageWithParam($newId);
}

// Check if an item needs to be deleted
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deletetargetID'], $_POST['editDelete'])) {
    $deleteId = $_POST['deletetargetID'];
    foreach ($menu_json as $key => $item) {
        if ($item['id'] == $deleteId) {
            unset($menu_json[$key]);  // Remove the item from the array
            break;
        }
    }
    $menu_json = array_values($menu_json);
    file_put_contents('json/menu.json', json_encode($menu_json, JSON_PRETTY_PRINT));
    header("Location: edit.php");
    exit;
}

function getAncestorIds($items, $currentId) {
    $parents = [];
    $parentId = null;
    // Continue looping until there are no more parents
    while ($currentId) {
        foreach ($items as $item) {
            if ($item['id'] == $currentId) {
                $parentId = $item['parent_id'];
                // If the parent ID is 0, we've reached the top of the hierarchy
                if ($parentId == 0) {
                    return $parents;
                }
                $parents[] = $parentId;
                $currentId = $parentId; // Set current ID to parent ID for next loop iteration
            }
        }
    }
    return $parents;
}

if (isset($_GET['p'])) {
    $currentPageId = (int)$_GET['p']; // Cast to integer for security and consistency
    $ancestors = getAncestorIds($menu_json, $currentPageId);
}
else{
  $currentPageId = -1;
  $ancestors = [];
}

global $currentPageName;

function buildMenu($menu_json, $parentId = 0, $currentPageId = null, $ancestors = []) {
    global $ancestors; // Ensure $ancestors is accessible within the function
    global $currentPageName;

    if ($currentPageId === null && isset($_GET['p'])) {
        $currentPageId = (int)$_GET['p']; // Cast to integer for security and consistency
        // It's better to move the $ancestors population outside this function to avoid redundant calls,
        // but make sure it's populated before calling buildMenu.
    }

    $result = "";
    foreach ($menu_json as $item) {
        if ($item['parent_id'] == $parentId) {
            $isCurrentPage = ($item['id'] == $currentPageId);
            $isAncestorOrCurrent = in_array($item['id'], $ancestors) || $isCurrentPage;

            // Apply 'expanded' class if the item is the current page or an ancestor of the current page
            $classNames = 'menu-item';

            if (isset($item['level']) && $item['level'] == "1"){
              $classNames .= " menu-level-1";
            }
            else if (isset($item['level']) && $item['level'] == "2"){
              $classNames .= " menu-level-2";
            }

            if ($isAncestorOrCurrent) {
                $classNames .= ' expanded';
            }
            if ($isCurrentPage) {
                $classNames .= ' current-item';
                $currentPageName = $item['name'];
            }

            // Recursively build menu for children
            $childrenHtml = buildMenu($menu_json, $item['id'], $currentPageId, $ancestors);

            if ($childrenHtml) {
                $classNames .= ' has-children';
                $indicator = '<span class="indicator"></span>'; // Adjust based on whether the item is expanded or not
            } else {
                $indicator = '<span class="indicator indicator-page"></span>'; // Use the page icon for leaf nodes
            }

            $result .= sprintf(
                '<div class="%s" data-page-id="%d" title="ID: %s"><span class="name">%s %s</span>%s</div>',
                $classNames, $item['id'], $item['id'], $indicator, $item['name'], $childrenHtml ? "<div class=\"content\">$childrenHtml</div>" : ""
            );
        }
    }
    return $result;
}

$menuHtml = buildMenu($menu_json, 0, isset($_GET['p']) ? $_GET['p'] : null);

?>

<div class="sidebar" id="menu">
    <?php echo $menuHtml;?>
</div>

<?php

global $rowIndex;
$rowIndex = 0;

// Function to display page content
function generate_content($content, $currentPageName) {
    global $rowIndex;
    // Header
    $html_string = '<h3 style="padding-left:10px;">'.$currentPageName.'</h2><hr style="padding-left:20px;">';
    $html_string .= "<table class='table_layout'>";
    foreach ($content['rows'] as $index => $row) {
        $rowIndex = $index + 1; // Start index from 1 for consistency with manual additions
        $textId = 'text_' . $rowIndex;
        $imageId = 'image_' . $rowIndex;
        $imageUrlId = 'image_url_' . $rowIndex;
        $newImageId = 'new_image_' . $rowIndex;

        $html_string .= "<tr>";
        $html_string .= '<td class="td_layout"><textarea class="editor" id="' . $textId . '" name="' . $textId . '">' . htmlspecialchars($row['text']) . '</textarea><br><br><button type="button" onclick="deleteRow(this)">Delete Row</button></td>';
        $html_string .= '<td class="td_layout" style="width:30%;">';
        if (!empty($row['image'])) {
            $html_string .= sprintf('<img src="%s"><br><br>', htmlspecialchars($row['image'], ENT_QUOTES, 'UTF-8'));
        }
        $html_string .= '<input type="hidden" name="' . $imageUrlId . '" value="' . htmlspecialchars($row['image'] ?? '') . '">';
        $html_string .= '<div class="dropZone">Drag and drop image here<input type="file" class="fileInput" name="' . $newImageId . '" hidden></div>';
        $html_string .= '</td>';
        $html_string .= "</tr>";
        $html_string .= '<tr><td class="td_layout" colspan=2><hr></td></tr>';
    }
    $html_string .= "</table>";
    $html_string .= '<div style="padding:20px;"><button type="button" onclick="addRow()" >Add Row</button></div><br>';
    return $html_string;
}

// Load the content
if (isset($_GET['p'])){
  $zero_padded_rowIndex = sprintf("%05d", $_GET['p']);
  $page_key = $zero_padded_rowIndex ?? '00001';  // Default to '001' if no page specified
  $content_file = 'json/page_' . $page_key . '.json';
  $content_json = read_json($content_file);
  $content_html = generate_content($content_json, $currentPageName);
}
else{
  $content_html = "<br>Please select a page to view its content.";
}

?>

<div class="page-content">
  <form id="contentForm" action="?page=<?php echo @$page_key || "";?>&action=save" method="post" enctype="multipart/form-data">
    <?php echo $content_html; ?>
  </form>
</div>

<?php
// Generate HTML for a dropdown list of pages
function generatePagesDropdown($pages, $elementname, $parentId = 0, $level = 0, $root_label="No Parent", $root_value="0", $required="") {
    $html = ($level === 0) ? '<select class="full-width" name="' . $elementname . '" '.$required.'>' : ''; // Start select tag only at top level
    if ($level === 0) {
        $html .= '<option value="'.$root_value.'">'.$root_label.'</option>'; // Default option at the top level
    }

    foreach ($pages as $page) {
        if ($page['parent_id'] == $parentId) {
            // Indentation based on level (you can use "--" or any other prefix to indicate depth)
            $prefix = str_repeat('&nbsp;', $level * 4) . str_repeat('-', $level);

            $html .= sprintf('<option value="%d">%s %s</option>', $page['id'], $prefix, htmlspecialchars($page['name']));

            // Recursively add children
            $html .= generatePagesDropdown($pages, $elementname, $page['id'], $level + 1);
        }
    }

    $html .= ($level === 0) ? '</select>' : ''; // Close select tag only at top level
    return $html;
}

function generateLevelsDropdown($users, $elementName) {
    $html = '<select class="full-width" name="' . $elementName . '">';
    foreach ($users as $level => $label) {
        $html .= sprintf('<option value="%s">%s</option>', $level, ucfirst($label));
    }
    $html .= '</select>';
    return $html;
}

?>

<!-- Modal Structure -->
<div id="editModal">
    <div class="modal-container">
        <div class="modal-header">
            <h3>Edit Menu Item</h3>
        </div>
        <div class="modal-body">
            <form method="POST">
                <p>Delete Target ID:</p>
                <?php echo generatePagesDropdown($menu_json, $elementname="deletetargetID", $parentId = 0, $level = 0, $root_label="Select...", $root_value="", $required="required");?>
                <input name="editDelete" type="submit" class="btn" value="Delete">
            </form>

            <hr style="margin: 20px 0px;">

            <form method="POST">
                <p>Edit Target ID:</p>
                <?php echo generatePagesDropdown($menu_json, $elementname="edittargetID", $parentId = 0, $level = 0, $root_label="Select...", $root_value="", $required="required");?>
                <p>New Parent ID:</p>
                <?php echo generatePagesDropdown($menu_json, $elementname="editparentId", $parentId = 0, $level = 0, $root_label="No Parent", $root_value="0", $required="required");?>
                <p>New Title:</p>
                <input type="text" name="editName" class="full-width">
                <p>New Access Level:</p>
                <?php echo generateLevelsDropdown(["open", "integrator", "developer"], $elementname="editLevel");?>
                <input name="editSave" type="submit" class="btn" value="Save">
            </form>
        </div>
        <div class="modal-footer">
            <button onclick="document.getElementById('editModal').style.display='none'" class="btn">Close</button>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="addModal">
    <div class="modal-container">
        <div class="modal-header">
            <h3>Add a New Page</h3>
        </div>
        <div class="modal-body">
          <form method="POST">
              <p>Add page with title:</p>
              <input type="text" name="addnewName" class="full-width" required>
              <p>Parent ID of new page:</p>
              <?php echo generatePagesDropdown($menu_json, $elementname="addparentId");?>
              <p>Access Level of new page:</p>
              <?php echo generateLevelsDropdown(["open", "integrator", "developer"], $elementname="addLevel");?>
              <input type="submit" class="btn" value="Add">
          </form>
        </div>
        <div class="modal-footer">
            <button onclick="document.getElementById('addModal').style.display='none'" class="btn">Close</button>
        </div>
    </div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
  document.querySelectorAll('.dropZone').forEach(function(dropZoneElem) {
      dropZoneElem.addEventListener('click', function() {
          this.querySelector('.fileInput').click();
      });

      dropZoneElem.addEventListener('dragover', function(event) {
          event.preventDefault();
          event.stopPropagation();
          this.style.backgroundColor = '#F0F8FF';
      }, false);

      dropZoneElem.addEventListener('dragleave', function(event) {
          event.preventDefault();
          event.stopPropagation();
          this.style.backgroundColor = '';
      }, false);

      dropZoneElem.addEventListener('drop', function(event) {
          event.preventDefault();
          event.stopPropagation();
          this.style.backgroundColor = '';

          var files = event.dataTransfer.files;
          if (files.length) {
              var file = files[0];
              this.querySelector('.fileInput').files = files;
              this.innerHTML = 'File dropped: ' + file.name;  // Update the drop zone text
          }
      }, false);
  });
});

</script>

<script>

function reloadPageWithParam(pValue) {
    var newUrl = new URL(window.location.href);
    newUrl.searchParams.set('p', pValue); // Set or update the 'p' query parameter
    window.location.href = newUrl.href;
}

</script>

<script>
  document.querySelectorAll('.menu-item').forEach(function(item) {
      item.addEventListener('click', function(e) {
          e.stopPropagation(); // Prevents triggering clicks on parent items

          // Check if it is a leaf node (has the 'indicator-page' class)
          var isLeafNode = this.querySelector('.indicator-page');

          // Check if it has content (children)
          var content = this.querySelector('.content');

          // If it's a leaf node, navigate to the URL
          if (isLeafNode && !content) {
              var pageId = this.getAttribute('data-page-id'); // Assuming you've set this attribute
              window.location.href = `edit.php?p=${pageId}`;
          } else if (content) {
              // Toggle expansion for non-leaf nodes
              this.classList.toggle('expanded');
              var indicator = this.querySelector('.indicator');
              if (this.classList.contains('expanded')) {
                  indicator.style.backgroundImage = "url('static/img/caret_down.png')";
              } else {
                  indicator.style.backgroundImage = "url('static/img/caret_right.png')";
              }
          }
      });
  });

  // Set initial state of indicators
  document.querySelectorAll('.menu-item.has-children .indicator').forEach(function(indicator) {
      indicator.style.backgroundImage = "url('static/img/caret_right.png')";
  });
  document.querySelectorAll('.menu-item:not(.has-children) .indicator').forEach(function(indicator) {
      indicator.style.backgroundImage = "url('static/img/page.png')"; // Assuming you want the dot to be a page icon
  });

  // Add dot for items without children
  document.querySelectorAll('.menu-item').forEach(function(item) {
      if (!item.querySelector('.content')) {
          let indicator = item.querySelector('.indicator');
          if(indicator) indicator.textContent = ''; // Dot for no children
      } else {
          item.classList.add('has-children');
      }
  });
</script>

<script>
document.addEventListener('DOMContentLoaded', function() {
    // Update indicators for expanded items
    document.querySelectorAll('.menu-item.expanded').forEach(function(item) {
        const indicator = item.querySelector('.indicator');
        // Check if it has a content block to expand
        if (item.querySelector('.content')) {
            indicator.style.backgroundImage = "url('static/img/caret_down.png')";
        }
    });
  });
</script>

<script>
  tinymce.init({
    selector: 'textarea',  // Change this value according to your HTML
    menu: {
      file: { title: 'File', items: '' },
      edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace' },
      view: { title: 'View', items: '' },
      insert: { title: 'Insert', items: 'link hr codeformat' },
      format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | styles blocks fontsize align lineheight | forecolor backcolor | language | removeformat' },
      tools: { title: 'Tools', items: '' },
      table: { title: 'Table', items: 'inserttable | cell row column | advtablesort | tableprops deletetable' },
      help: { title: 'Help', items: '' }
    },
    plugins: 'lists advlist link table',
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    image_advtab: true,
    setup: function (editor) {
      editor.on('change', function () {
        editor.save(); // Ensures content is saved to textarea on change
      });
    }
  });
  // TinyMCE will now save the data into textarea
  tinymce.triggerSave();
</script>

<script>

  let rowCounter = <?php echo $rowIndex;?>;  // Initialize a global counter

  function addRow() {
      const table = document.querySelector('#contentForm table');
      const newRow = table.insertRow(-1);
      const textCell = newRow.insertCell(0);
      const imageCell = newRow.insertCell(1);

      // Increment the counter and use it to create unique IDs
      rowCounter++;
      const textareaId = 'text_' + rowCounter;
      const fileId = 'new_image_' + rowCounter;

      textCell.innerHTML = '<textarea class="editor" id="' + textareaId + '"></textarea><br><br><button type="button" onclick="deleteRow(this)">Delete Row</button>';
      imageCell.innerHTML = '<input type="file" id="' + fileId + '" name="' + fileId + '">';

      // Reinitialize TinyMCE for the new textarea
      tinymce.init({ selector: '#' + textareaId });
  }

  function deleteRow(button) {
      // Optional: Update global counter or adjust it based on the situation
      const row = button.parentNode.parentNode;
      row.parentNode.removeChild(row);
  }

  function saveContent() {
    // Trigger TinyMCE to update the textarea elements
    tinymce.triggerSave();

    const formElement = document.getElementById('contentForm');
    const data = new FormData(formElement);

    // Scan and manually add data from elements with IDs starting with 'mce_'
    document.querySelectorAll('[id^="text_"]').forEach(element => {
        if (element.value !== undefined) {
            data.append(element.id, element.value); // Append using the ID as the key
        }
    });

    // Log FormData contents for debugging
    for (let [key, value] of data.entries()) {
        console.log(key, value); // Confirming the data to be sent
    }

    fetch(`save.php?page=<?php echo $page_key; ?>&action=save`, {
        method: 'POST',
        body: data
    }).then(response => response.text()).then(html => {
        console.log('Server response:', html);
        reloadWithSuccess(); // Reload page to reflect changes
    });
}

function reloadWithSuccess() {
    const currentUrl = window.location.href;
    const newUrl = new URL(currentUrl);
    newUrl.searchParams.set('saved', 'true'); // Append or update a 'saved' parameter
    window.location.href = newUrl.href; // Change the location to the new URL with the parameter
}

</script>

<script>
document.addEventListener("DOMContentLoaded", function() {
    var saveTimestamp = document.getElementById('save_timestamp');
    var saveButton = document.getElementById('save_button');
    if (saveTimestamp.style.display !== 'none') {
        setTimeout(function() {
            saveTimestamp.style.display = 'none';
            saveButton.style.display = '';
        }, 5000); // Hide after 5000 milliseconds (5 seconds)
    }
});
</script>

<script>
document.getElementById('editButton').addEventListener('click', function() {
    document.getElementById('editModal').style.display = 'block';
});
</script>

<script>
document.getElementById('addButton').addEventListener('click', function() {
    document.getElementById('addModal').style.display = 'block';
});
</script>

<script>
document.getElementById('editModal').addEventListener('click', function(event) {
    if (event.target === this) {
        this.style.display = 'none';
        console.log("Closing modal");
    }
});
</script>

<script>
document.getElementById('addModal').addEventListener('click', function(event) {
    if (event.target === this) {
        this.style.display = 'none';
    }
});
</script>

</body>
</html>
