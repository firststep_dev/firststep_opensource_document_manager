<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>FirstStep.ai Documentation Hub</title>
    <link rel="stylesheet" href="static/css/style.css" referrerpolicy="origin" crossorigin="anonymous">
    <script src="https://cdn.tiny.cloud/1/5715mm0o7p7ydnjzawqbul9oegvvivmtqawrkbosn3s8ugp5/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <link rel="icon" type="image/png" href="static/img/favicon.png">
</head>
<body>

  <?php include_once("vars.php");?>

  <div class="header">
      <div style="display: flex; align-items: center;">
          <a href="<?php echo $BASE_URL;?>"><img src="static/img/logo_firststep.svg" style="height:50px;"></a>
          <span style="font-size: 18px;">Documentation Hub</span>
      </div>
      <div class="version">
          User: <?php echo ucwords($_SESSION['user_role']); ?> (<?php echo $loginLogoutButton; ?>)<br>
          Platform Version: <?php echo $SOFTWARE_VERSION; ?><br>
          PHP Version: <?php echo $PHP_VERSION; ?>
      </div>
  </div>
