<?php
session_start();

if (!isset($_SESSION['user_role'])) {
    $_SESSION['user_role'] = "open";
}

if (in_array($_SESSION['user_role'], ["editor", "developer", "integrator"])) {
    $loginLogoutButton = '<a href="auth.php?logout=true">Logout</a>';
} else {
    $loginLogoutButton = '<a href="auth.php">Login</a>';
}
?>

<?php

include_once("vars.php");
ini_set('display_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>

<?php

function log_message($message) {
    $log_file = 'logs/index.log';
    $current_time = date('Y-m-d H:i:s');

    // Get the full URL
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
    $url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $log_entry = $current_time . ' - ' . $message . ' - URL: ' . $url . "\n";
    file_put_contents($log_file, $log_entry, FILE_APPEND);
}

log_message("Page call (index.php)");

?>

<?php include_once("header.php");?>

  <div class="subheader">
      Product:
      <select id="product-select" disabled>
          <option value="" selected>CRATE INSPECTOR</option>
      </select>

      Version:
      <select id="version-select" disabled>
          <option value="" selected>2024</option>
      </select>
  </div>


<?php

function read_json($filename) {
    if (!file_exists($filename)) {
        // Create a blank template and save it if the file doesn't exist
        $data = ['rows' => []];
        file_put_contents($filename, json_encode($data, JSON_PRETTY_PRINT));
        return $data;
    }
    $json = file_get_contents($filename);
    return json_decode($json, true);
}

// Function to reload the page with a specific 'p' value
function reloadPageWithParam($pValue) {
    $baseUrl = $_SERVER['PHP_SELF'];
    $queryParams = $_GET;
    $queryParams['p'] = $pValue;
    $queryString = http_build_query($queryParams);
    $fullUrl = $baseUrl . '?' . $queryString;
    header("Location: $fullUrl");
    exit; // Make sure to call exit after header redirection
}

$menu_json = read_json('json/menu.json');

function getAncestorIds($items, $currentId) {
    $parents = [];
    $parentId = null;
    // Continue looping until there are no more parents
    while ($currentId) {
        foreach ($items as $item) {
            if ($item['id'] == $currentId) {
                $parentId = $item['parent_id'];
                // If the parent ID is 0, we've reached the top of the hierarchy
                if ($parentId == 0) {
                    return $parents;
                }
                $parents[] = $parentId;
                $currentId = $parentId; // Set current ID to parent ID for next loop iteration
            }
        }
    }
    return $parents;
}

if (isset($_GET['p'])) {
    $currentPageId = (int)$_GET['p']; // Cast to integer for security and consistency
    $ancestors = getAncestorIds($menu_json, $currentPageId);
}
else{
  $currentPageId = -1;
  $ancestors = [];
}

global $currentPageName;

function buildMenu($menu_json, $parentId = 0, $currentPageId = null, $ancestors = []) {
    global $ancestors; // Ensure $ancestors is accessible within the function
    global $currentPageName;

    if ($currentPageId === null && isset($_GET['p'])) {
        $currentPageId = (int)$_GET['p']; // Cast to integer for security and consistency
        // It's better to move the $ancestors population outside this function to avoid redundant calls,
        // but make sure it's populated before calling buildMenu.
    }

    $show_page_levels = ["0"]; # Open access
    if ($_SESSION["user_role"] == "integrator"){
      $show_page_levels = ["0", "1"];
    }
    else if ($_SESSION["user_role"] == "developer"){
      $show_page_levels = ["0", "1", "2"];
    }
    else if ($_SESSION["user_role"] == "editor"){
      $show_page_levels = ["0", "1", "2"];
    }

    $result = "";
    foreach ($menu_json as $item) {
        if (isset($item["level"]) && !in_array($item["level"], $show_page_levels)) {
          continue;
        }
        if ($item['parent_id'] == $parentId) {
            $isCurrentPage = ($item['id'] == $currentPageId);
            $isAncestorOrCurrent = in_array($item['id'], $ancestors) || $isCurrentPage;

            // Apply 'expanded' class if the item is the current page or an ancestor of the current page
            $classNames = 'menu-item';

            if (isset($item['level']) && $item['level'] == "1"){
              $classNames .= " menu-level-1";
            }
            else if (isset($item['level']) && $item['level'] == "2"){
              $classNames .= " menu-level-2";
            }

            if ($isAncestorOrCurrent) {
                $classNames .= ' expanded';
            }
            if ($isCurrentPage) {
                $classNames .= ' current-item';
                $currentPageName = $item['name'];
            }

            // Recursively build menu for children
            $childrenHtml = buildMenu($menu_json, $item['id'], $currentPageId, $ancestors);

            if ($childrenHtml) {
                $classNames .= ' has-children';
                $indicator = '<span class="indicator"></span>'; // Adjust based on whether the item is expanded or not
            } else {
                $indicator = '<span class="indicator indicator-page"></span>'; // Use the page icon for leaf nodes
            }

            $result .= sprintf(
                '<div class="%s" data-page-id="%d" title="ID: %s"><span class="name">%s %s</span>%s</div>',
                $classNames, $item['id'], $item['id'], $indicator, $item['name'], $childrenHtml ? "<div class=\"content\">$childrenHtml</div>" : ""
            );
        }
    }
    return $result;
}

$menuHtml = buildMenu($menu_json, 0, isset($_GET['p']) ? $_GET['p'] : null);

?>

<div class="sidebar" id="menu">
    <?php echo $menuHtml;?>
</div>

<?php

global $rowIndex;
$rowIndex = 0;

// Function to display page content
function generate_content($content, $currentPageName) {
    global $rowIndex;
    // Header
    $html_string = '<h3 style="padding-left:10px;">'.$currentPageName.'</h2><hr style="padding-left:20px;">';
    $html_string .= "<table class='table_layout'>";
    foreach ($content['rows'] as $index => $row) {
        $rowIndex = $index + 1; // Start index from 1 for consistency with manual additions
        $textId = 'text_' . $rowIndex;
        $imageId = 'image_' . $rowIndex;
        $imageUrlId = 'image_url_' . $rowIndex;
        $newImageId = 'new_image_' . $rowIndex;
        $html_string .= "<tr>";
        if (strlen($row['image'] ?? '') == 0) {
          $html_string .= '<td colspan=2 class="td_layout">' . ($row['text']) . '</td>';
        }
        else{
          $html_string .= '<td class="td_layout">' . ($row['text']) . '</td>';
          $html_string .= '<td class="td_layout" style="width:30%;">';
          $html_string .= '   <img src="' . $row['image'] . '">';
        }
        $html_string .= '</td>';
        $html_string .= "</tr>";
        $html_string .= '<tr><td class="td_layout" colspan=2><hr></td></tr>';
    }
    $html_string .= "</table>";
    return $html_string;
}

// Load the content
if (isset($_GET['p'])){
  $zero_padded_rowIndex = sprintf("%05d", $_GET['p']);
  $page_key = $zero_padded_rowIndex ?? '00001';  // Default to '001' if no page specified
  $content_file = 'json/page_' . $page_key . '.json';
  $content_json = read_json($content_file);
  $content_html = generate_content($content_json, $currentPageName);
}
else{
  $content_html = "<br>Please select a page to view its content.";
}

?>

<div class="page-content">
    <?php echo $content_html; ?>
</div>

<script>
  document.querySelectorAll('.menu-item').forEach(function(item) {
      item.addEventListener('click', function(e) {
          e.stopPropagation(); // Prevents triggering clicks on parent items

          // Check if it is a leaf node (has the 'indicator-page' class)
          var isLeafNode = this.querySelector('.indicator-page');

          // Check if it has content (children)
          var content = this.querySelector('.content');

          // If it's a leaf node, navigate to the URL
          if (isLeafNode && !content) {
              var pageId = this.getAttribute('data-page-id'); // Assuming you've set this attribute
              window.location.href = `index.php?p=${pageId}`;
          } else if (content) {
              // Toggle expansion for non-leaf nodes
              this.classList.toggle('expanded');
              var indicator = this.querySelector('.indicator');
              if (this.classList.contains('expanded')) {
                  indicator.style.backgroundImage = "url('static/img/caret_down.png')";
              } else {
                  indicator.style.backgroundImage = "url('static/img/caret_right.png')";
              }
          }
      });
  });

  // Set initial state of indicators
  document.querySelectorAll('.menu-item.has-children .indicator').forEach(function(indicator) {
      indicator.style.backgroundImage = "url('static/img/caret_right.png')";
  });
  document.querySelectorAll('.menu-item:not(.has-children) .indicator').forEach(function(indicator) {
      indicator.style.backgroundImage = "url('static/img/page.png')"; // Assuming you want the dot to be a page icon
  });

  // Add dot for items without children
  document.querySelectorAll('.menu-item').forEach(function(item) {
      if (!item.querySelector('.content')) {
          let indicator = item.querySelector('.indicator');
          if(indicator) indicator.textContent = ''; // Dot for no children
      } else {
          item.classList.add('has-children');
      }
  });
</script>

<script>
document.addEventListener('DOMContentLoaded', function() {
    // Update indicators for expanded items
    document.querySelectorAll('.menu-item.expanded').forEach(function(item) {
        const indicator = item.querySelector('.indicator');
        // Check if it has a content block to expand
        if (item.querySelector('.content')) {
            indicator.style.backgroundImage = "url('static/img/caret_down.png')";
        }
    });
  });
</script>

</body>
</html>
