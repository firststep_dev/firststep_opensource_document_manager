<?php

$SOFTWARE_VERSION = "2024.05.14";
$PHP_VERSION = phpversion();
$APP_NAME = "CRATE INSPECTOR";
$APP_VERSION = array(
    "CRATE INSPECTOR" => ["2023", "2024"],
    "INTELLIGENT SAFETY" => ["2023"]
);
$APP_USERS = [
    'integrator' => 'integrator#',
    'developer' => 'developer$',
    'editor' => 'editor%'
];

$BASE_URL = "http://localhost/firststep_opensource_document_manager";

?>
