<?php

// Function to log messages
function log_message($message) {
    $log_file = 'logs/save.log';
    $current_time = date('Y-m-d H:i:s');

    $clean_message = preg_replace('/\s+/', ' ', trim($message)); // Replaces multiple whitespaces with a single space
    $log_entry = $current_time . ' - ' . $clean_message . "\n";
    file_put_contents($log_file, $log_entry, FILE_APPEND);
}

log_message("==================================");
$scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
$url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
log_message("URL: ".$url);
log_message("REQUEST_METHOD: ".$_SERVER['REQUEST_METHOD']);
log_message("GET: " . print_r($_GET,true));
log_message("POST: " . print_r($_POST,true));
log_message("FILES: " . print_r($_FILES,true));

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $page_key = $_GET['page'] ?? 'default_page';
    $content_file = 'json/page_' . $page_key . '.json';
    $content_json = ['rows' => []];

    foreach ($_POST as $key => $value) {
        if (strpos($key, 'text_') === 0) {
            $index = substr($key, 5);
            $text = $value;
            $image_url_key = 'image_url_' . $index;
            $new_image_key = 'new_image_' . $index;
            $image_path = $_POST[$image_url_key]; // Default to existing image URL

            // Check for a new image upload
            if (isset($_FILES[$new_image_key]) && $_FILES[$new_image_key]['error'] == 0) {
                $image_info = $_FILES[$new_image_key];
                $fileExtension = strtolower(pathinfo($image_info['name'], PATHINFO_EXTENSION));
                $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp'];  // Define allowed extensions

                if (in_array($fileExtension, $allowedExtensions)) {  // Corrected syntax for in_array check
                    $newFilename = "page_" . $page_key . "_" . date('YmdHis') . '_' . dechex(rand(0, 1048575)) . '.' . $fileExtension;
                    $upload_path = 'uploads/' . $newFilename;

                    if (move_uploaded_file($image_info['tmp_name'], $upload_path)) {
                        log_message("New image uploaded successfully: " . $upload_path);
                        $image_path = $upload_path; // Update image URL to new uploaded file
                    } else {
                        log_message("Failed to move uploaded file for row " . $index);
                    }
                } else {
                    log_message("Invalid file extension for row " . $index);
                }
            } else {
                if (isset($_FILES[$new_image_key])) {
                    log_message("Upload error or no file uploaded for row " . $index . "; Error: " . print_r($_FILES[$new_image_key]['error'], true));
                }
            }

            $content_json['rows'][] = ['text' => $text, 'image' => $image_path];
        }
    }

    if (file_put_contents($content_file, json_encode($content_json, JSON_PRETTY_PRINT))) {
        log_message("Content saved successfully to " . $content_file);
        echo 'Content saved successfully!';
    } else {
        log_message("Failed to save content to " . $content_file);
        echo 'Failed to save content.';
    }
    exit;
}


?>
