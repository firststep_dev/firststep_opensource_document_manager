<?php
session_start();

if (!isset($_SESSION['user_role'])) {
    $_SESSION['user_role'] = "open";
}

if (in_array($_SESSION['user_role'], ["editor", "developer", "integrator"])) {
    $loginLogoutButton = '<a href="auth.php?logout=true">Logout</a>';
} else {
    $loginLogoutButton = '<a href="auth.php">Login</a>';
}
?>

<?php

include_once("vars.php");
ini_set('display_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>

<?php

function log_message($message) {
    $log_file = 'logs/index.log';
    $current_time = date('Y-m-d H:i:s');

    // Get the full URL
    $scheme = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
    $url = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $log_entry = $current_time . ' - ' . $message . ' - URL: ' . $url . "\n";
    file_put_contents($log_file, $log_entry, FILE_APPEND);
}

log_message("Page call (index.php)");

?>

<?php include_once("header.php");?>

  <div class="subheader">
      Product:
      <select id="product-select">
          <option value="" selected>Select...</option>
          <?php foreach ($APP_NAME as $product): ?>
              <option value="<?php echo $product; ?>"><?php echo $product; ?></option>
          <?php endforeach; ?>
      </select>

      Version:
      <select id="version-select" disabled>
          <option value="" selected>Select...</option>
      </select>
  </div>

  <script>
    document.getElementById('product-select').addEventListener('change', function() {
        var product = this.value;
        var versions = <?php echo json_encode($APP_VERSION); ?>;
        var versionSelect = document.getElementById('version-select');
        versionSelect.innerHTML = '<option value="" selected>Select...</option>'; // Reset version options
        versionSelect.disabled = !versions[product];
        if (versions[product]) {
            versions[product].forEach(function(version) {
                var option = document.createElement('option');
                option.value = version;
                option.text = version;
                versionSelect.appendChild(option);
            });
        }
    });

    document.getElementById('version-select').addEventListener('change', function() {
        var product = document.getElementById('product-select').value.toLowerCase().replace(/ /g, '_');
        var version = this.value;
        if (product && version) {
            var currentUrl = window.location.href;
            var baseUrl = currentUrl.substring(0, currentUrl.lastIndexOf('/'));
            window.location.href = baseUrl + '/content/' + product + '/' + version + '/index.php';
        }
    });
  </script>

<?php

$menuHtml = "";

?>

<div class="sidebar" id="menu">
    <?php echo $menuHtml;?>
</div>

<?php

$content_html = "<br>Please select a <b>product</b> and <b>version</b>.";

?>

<div class="page-content">
    <?php echo $content_html; ?>
</div>

</body>
</html>
