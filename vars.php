<?php

$SOFTWARE_VERSION = "2024.05.14";
$PHP_VERSION = phpversion();
$APP_NAME = ["CRATE INSPECTOR", "INTELLIGENT SAFETY"];
$APP_VERSION = array(
    "CRATE INSPECTOR" => ["2024"],
    "INTELLIGENT SAFETY" => ["2024"]
);
$APP_USERS = [
    'integrator' => 'integrator#',
    'developer' => 'developer$',
    'editor' => 'editor%'
];

?>
